import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inicial/providers/colores_provider.dart';
import 'package:inicial/widgets/my_floatingaction.dart';
import 'package:inicial/widgets/my_text.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //final ColoresProvider coloresProvider = Provider.of<ColoresProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Aplicación con Providers'),
      ),
      body: Center(child: MyText()),
      floatingActionButton: MyFloatingAction(),
    );
  }
}
