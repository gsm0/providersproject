import 'package:flutter/material.dart';
import 'package:inicial/providers/colores_provider.dart';
import 'package:inicial/providers/count_provider.dart';
import 'package:provider/provider.dart';

class MyFloatingAction extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ColoresProvider coloresProvider =
        Provider.of<ColoresProvider>(context);
    final CountProvider countProvider = Provider.of<CountProvider>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        FloatingActionButton(
            child: Icon(Icons.ac_unit_outlined),
            backgroundColor: Colors.red,
            onPressed: () {
              coloresProvider.color = 'Rojo';
              coloresProvider.colorBase = Colors.red;
              countProvider.contar();
              countProvider.colorBase = coloresProvider.colorBase;
            }),
        SizedBox(
          height: 20.0,
        ),
        FloatingActionButton(
            child: Icon(Icons.access_alarm),
            backgroundColor: Colors.blue,
            onPressed: () {
              coloresProvider.color = 'Azul';
              coloresProvider.colorBase = Colors.blue;
              countProvider.contar();
              countProvider.colorBase = coloresProvider.colorBase;
            })
      ],
    );
  }
}
