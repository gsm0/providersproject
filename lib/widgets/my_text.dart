import 'package:flutter/material.dart';
import 'package:inicial/providers/colores_provider.dart';
import 'package:inicial/providers/count_provider.dart';
import 'package:provider/provider.dart';

class MyText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Consumer<ColoresProvider>(builder: (context, coloresProvider, _) {
          return Text(coloresProvider.color,
              style:
                  TextStyle(color: coloresProvider.colorBase, fontSize: 30.0));
        }),
        Consumer<CountProvider>(builder: (context, countProvider, _) {
          return Text(countProvider.contador.toString(),
              style: TextStyle(color: countProvider.colorBase, fontSize: 30));
        }),
/*         Text(
          'Count:' + counter().toString(),
          style: TextStyle(fontSize: 30),
        ) */
      ],
    );
  }
}
