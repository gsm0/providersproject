import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CountProvider with ChangeNotifier {
  int _contador = 0;
  Color _colorBase = Colors.black;

  int contar() {
    _contador++;
    notifyListeners();
    return _contador;
  }

  get contador {
    return _contador;
  }

  get colorBase {
    return _colorBase;
  }

  set colorBase(Color colorBase) {
    this._colorBase = colorBase;
    notifyListeners();
  }

  /* set contadorS(int contador) {
    this._contador = contador + 1;
  } */
}
