import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColoresProvider with ChangeNotifier {
  String _color = 'ROJO';
  Color _colorBase = Colors.black;

  get color {
    return _color;
  }

  set color(String color) {
    _color = color;
    notifyListeners();
  }

  get colorBase {
    return _colorBase;
  }

  set colorBase(Color colorBase) {
    this._colorBase = colorBase;
    notifyListeners();
  }
}
