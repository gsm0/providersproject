import 'package:flutter/material.dart';
import 'package:inicial/pages/home_page.dart';
import 'package:inicial/providers/colores_provider.dart';
import 'package:inicial/providers/count_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ColoresProvider()),
        ChangeNotifierProvider(create: (_) => CountProvider()),
      ],
      child: MaterialApp(
          title: 'Material App',
          initialRoute: 'home',
          routes: {'home': (context) => HomePage()}),
    );
  }
}
